package by.andrey.dbtest1.items.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ItemDto {

    private Long id;

    private String name;

    private String description;
}
