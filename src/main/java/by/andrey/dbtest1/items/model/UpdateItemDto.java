package by.andrey.dbtest1.items.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateItemDto {

    private String name;

    private String description;
}
