package by.andrey.dbtest1.items.dal.repository;

import by.andrey.dbtest1.items.dal.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<ItemEntity, Long> {

}
