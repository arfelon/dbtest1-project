package by.andrey.dbtest1.items.service;

import by.andrey.dbtest1.items.dal.entity.ItemEntity;
import by.andrey.dbtest1.items.dal.repository.ItemRepository;
import by.andrey.dbtest1.items.mapper.ItemMapper;
import by.andrey.dbtest1.items.model.CreateItemDto;
import by.andrey.dbtest1.items.model.ItemDto;
import by.andrey.dbtest1.items.model.UpdateItemDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ItemService {

    private final ItemMapper itemMapper;

    private final ItemRepository itemRepository;

    public List<ItemDto> getAllItems() {
        List<ItemEntity> entities = itemRepository.findAll();

        return itemMapper.toItemDto(entities);
    }

    public ItemDto getItemById(Long id) {
        ItemEntity entity = itemRepository.findById(id).orElse(null);

        return itemMapper.toItemDto(entity);
    }

    public ItemDto createItem(CreateItemDto item) {
        ItemEntity entity = itemMapper.toItemEntity(item);
        ItemEntity entityAfterSave = itemRepository.save(entity);

        return itemMapper.toItemDto(entityAfterSave);
    }

    public ItemDto updateItem(Long id, UpdateItemDto item) {
        ItemEntity entity = itemMapper.toItemEntity(id, item);
        ItemEntity entityAfterSave = itemRepository.save(entity);

        return itemMapper.toItemDto(entityAfterSave);
    }
}
