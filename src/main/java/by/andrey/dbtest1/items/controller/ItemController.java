package by.andrey.dbtest1.items.controller;

import by.andrey.dbtest1.items.model.CreateItemDto;
import by.andrey.dbtest1.items.model.ItemDto;
import by.andrey.dbtest1.items.model.UpdateItemDto;
import by.andrey.dbtest1.items.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/items")
public class ItemController {

    private final ItemService itemService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<ItemDto> getAllItems() {
        return itemService.getAllItems();
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ItemDto getItemById(@PathVariable(name = "id") Long id) {
        return itemService.getItemById(id);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ItemDto createItem(@RequestBody CreateItemDto item) {
        return itemService.createItem(item);
    }

    @PutMapping(value = "/{id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ItemDto updateItem(@PathVariable(name = "id") Long id, @RequestBody UpdateItemDto item) {
        return itemService.updateItem(id, item);
    }
}
