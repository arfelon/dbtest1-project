package by.andrey.dbtest1.items;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class DbTest1Application {

    public static void main(String[] args) {
        SpringApplication.run(DbTest1Application.class, args);
    }
}
