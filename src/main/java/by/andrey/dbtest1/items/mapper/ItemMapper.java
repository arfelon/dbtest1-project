package by.andrey.dbtest1.items.mapper;

import by.andrey.dbtest1.items.dal.entity.ItemEntity;
import by.andrey.dbtest1.items.model.CreateItemDto;
import by.andrey.dbtest1.items.model.ItemDto;
import by.andrey.dbtest1.items.model.UpdateItemDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ItemMapper {

    ItemDto toItemDto(ItemEntity entity);

    List<ItemDto> toItemDto(List<ItemEntity> entities);

    ItemEntity toItemEntity(CreateItemDto dto);

    ItemEntity toItemEntity(UpdateItemDto dto);

    default ItemEntity toItemEntity(Long id, UpdateItemDto dto) {
        ItemEntity entity = toItemEntity(dto);
        if (entity != null) {
            entity.setId(id);
        }

        return entity;
    }
}
